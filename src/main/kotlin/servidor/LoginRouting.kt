package servidor

import com.fasterxml.jackson.annotation.JsonIgnore
import com.google.gson.GsonBuilder
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.ktor.application.call
import io.ktor.auth.*
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*

fun Route.login() {
    route("/") {
        get {
            println("FERNANDO Entro")
            call.respondText("yes", ContentType.Text.Html)
        }
    }
    route("/login") {
        authentication {
            emailPasswordAuthetication()
        }
        post {
            println("FERNANDO LOGI")
            val principal = call.principal<User>() ?: throw IllegalStateException("Principal is null.")
            val token = generateToken(principal)
            println("FERNANDO $token")
            val gson = GsonBuilder().create()
            println("FERNANDO ${gson.toJson(LoginResponse(principal, token))}")
            call.respondText(gson.toJson(LoginResponse(principal, token)), ContentType.Application.Json, HttpStatusCode.OK)
        }
    }
}


fun AuthenticationPipeline.emailPasswordAuthetication() {
    intercept(AuthenticationPipeline.RequestAuthentication) { context ->
        val reques = call.receive<LoginRequest>()
        if (reques.email != null && reques.password != null && reques.email.toLowerCase().equals("fernandovs4@gmail.com") && reques.password.equals("fer123")) {
            val user = User(0, reques.email, reques.password, "")
            context.principal = user
        } else {
            context.challenge("Login", NotAuthenticatedCause.InvalidCredentials) {
                it.success()
                call.respond(HttpStatusCode.Unauthorized, "Invalid credential")
            }
        }
    }
}

fun generateToken(user: User): String {
    val expiration = LocalDateTime.now().plusHours(1).atZone(ZoneId.systemDefault())
    return Jwts.builder()
            .setSubject(user.id.toString())
            .setAudience("Qiita_sample")
            .setHeaderParam("typ", "JWT")
            .setExpiration(Date.from(expiration.toInstant()))
            .signWith(SignatureAlgorithm.HS256, "honmani secret na key")
            .compact()
}

data class LoginRequest(val email: String, val password: String)
data class LoginResponse(val user: User, val accessToken: String)
data class User(val id: Int, val name: String, val email: String, @JsonIgnore val encryptedPassword: String) : Principal