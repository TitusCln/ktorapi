package servidor

data class Product(
        var id: Int,
        var name: String,
        var imgUrl: String,
        var desc: String)

data class ResponseJson(val statusCode:Int,val playload:Any)