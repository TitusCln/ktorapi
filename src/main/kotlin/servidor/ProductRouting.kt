package servidor

import com.google.gson.GsonBuilder
import io.jsonwebtoken.Jwts
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.auth.*
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.route

const val REST_ENDPOINT = "/products"

fun Route.ProductsRoutes() {
    route("$REST_ENDPOINT/", HttpMethod.Get) {
        authentication {
            TokenAuthentication()
        }
        if (ProductRepo.getAll().isEmpty()) {
            println("La base de datos esta vacia, inicializala")
            (0..20).map {
                ProductRepo.add(Product(it, "Product $it", "https://picsum.photos/200?image= ${(0..200).random()}", "Descripción $it"))
            }
        }
        handle {
            val gson = GsonBuilder().create()
            call.respondText(gson.toJson(ProductRepo.getAll()), ContentType.Application.Json, HttpStatusCode.OK)
        }
    }
    route("$REST_ENDPOINT/{productId}", HttpMethod.Get) {
        authentication {
            TokenAuthentication()
        }
        handle {
            val id = call.parameters["productId"] ?: throw IllegalArgumentException("Parameter id not found")
            val gson = GsonBuilder().create()
            call.respondText(gson.toJson(ProductRepo.get(id.toInt())), ContentType.Application.Json, HttpStatusCode.OK)
        }
    }
    route("$REST_ENDPOINT/", HttpMethod.Post) {
        authentication {
            TokenAuthentication()
        }
        handle {
            println("ENTRO a POST")
            val product = call.receive<Product>()
            val gson = GsonBuilder().create()
            call.respondText(gson.toJson(ProductRepo.add(product)), ContentType.Application.Json, HttpStatusCode.OK)
        }
    }
    route("$REST_ENDPOINT/{productId}", HttpMethod.Delete) {
        authentication {
            TokenAuthentication()
        }
        handle {
            println(call.parameters)
            val id = call.parameters["productId"] ?: throw IllegalArgumentException("Parameter id not found")
            println(id)
            val gson = GsonBuilder().create()
            call.respondText((gson.toJson(ProductRepo.remove(id.toInt()))), ContentType.Application.Json, HttpStatusCode.OK)
        }
    }
}

fun AuthenticationPipeline.TokenAuthentication() {
    intercept(AuthenticationPipeline.CheckAuthentication) { context ->
        val header = call.request.parseAuthorizationHeader()
        val id = if (header != null && header.authScheme == "Bearer" && header is HttpAuthHeader.Single) {
            VerifyToken(header.blob)
        } else {
            null
        }

        if (id != null) {
            context.principal = AuthorizedUser(id)
        } else {
            context.challenge("TokenAuthentication", NotAuthenticatedCause.InvalidCredentials) {
                it.success()
                call.respond(HttpStatusCode.Unauthorized, "Invalid credential")
            }
        }
    }
}

fun VerifyToken(token: String): Int? {
    val jws = try {
        Jwts.parser().setSigningKey("honmani secret na key").parseClaimsJws(token)
    } catch (ex: Exception) {
        return null
    }

    return if (jws.body.audience == "Qiita_sample" && jws.body.subject != null) {
        jws.body.subject.toIntOrNull()
    } else {
        null
    }
}

data class AuthorizedUser(val Id: Int) : Principal

private suspend fun ApplicationCall.respondSuccessJson(value: Boolean = true) = respond("""{"success": "$value"}""")