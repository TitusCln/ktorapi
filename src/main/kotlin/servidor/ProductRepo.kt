package servidor

import java.util.concurrent.CopyOnWriteArraySet
import java.util.concurrent.atomic.AtomicInteger

object ProductRepo {

    private val idCounter = AtomicInteger()
    private val products = CopyOnWriteArraySet<Product>()

    fun add(p: Product): Product {
        if (products.contains(p)) {
            return products.find { it == p }!!
        }
        p.id = idCounter.incrementAndGet()
        products.add(p)
        return p
    }

    fun get(id: Int) = products.find { it.id == id } ?: throw  IllegalArgumentException("Not found")

    fun getAll() = products.toList()

    fun remove(p: Product) {
        if (!products.contains(p)) {
            throw IllegalArgumentException("No found")
        }
        products.remove(p)
    }

    fun remove(id: Int) = products.remove(get(id))

    fun clear() = products.clear()
}